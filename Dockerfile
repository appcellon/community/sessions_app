FROM ruby:2.6.2

RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
RUN mkdir /myapp

WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock

RUN gem install bundler:2.0.1
RUN bundle install

COPY . /myapp

EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]