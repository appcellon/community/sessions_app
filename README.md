# Goals for the next three months(June, July, August)


* 1. Code automation(Get an Idea) .md
	git push.
	GIT, TDD, CI, CD, Gitlab
* 2. Full stack development(Back end(API) to front end(React))
	Backend(Java, Python, Ruby, PHP, NodeJS and etc)
* 3. Containerization(Docker) and resource management(Docker swam or Kubernetes)
	
* 4. Build a Platform

# Setup 
## Prerequisites

* Install [ruby and rails](https://gorails.com/setup/ubuntu/18.04). Use ruby version 2.6.2
* Install Postgres

## Setup application

### Installation

Once you have the pre-requisistes setup:

    cd session-app
    gem install bundler:2.0.1 && bundle install 
    rails db:migrate RAILS_ENV=development

You have now installed all dependencies, run the `puma` server
   
    rails s

Browse to `localhost:3000/dashboard` to view the dashboard
